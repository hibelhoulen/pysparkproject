# import libraries
import pyspark.sql.functions as F
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


def create_coef_df(cols, lr, rf, gbt, grid):
    lr_feature_coef = lr.stages[2].coefficients.values.tolist()
    lr_dict = dict(list(zip(cols, lr_feature_coef)))
    lr_df = pd.DataFrame.from_dict(lr_dict, orient='index', columns=['coefficient']) \
        .sort_values('coefficient', ascending=False)

    rf_feature_coef = rf.stages[2].featureImportances.values.tolist()
    rf_dict = dict(list(zip(cols, rf_feature_coef)))
    rf_df = pd.DataFrame.from_dict(rf_dict, orient='index', columns=['coefficient']) \
        .sort_values('coefficient', ascending=False)

    gbt_feature_coef = gbt.stages[2].featureImportances.values.tolist()
    gbt_dict = dict(list(zip(cols, gbt_feature_coef)))
    gbt_df = pd.DataFrame.from_dict(gbt_dict, orient='index', columns=['coefficient']) \
        .sort_values('coefficient', ascending=False)

    grid_feature_coef = grid.stages[2].featureImportances.values.tolist()
    grid_dict = dict(list(zip(cols, rf_feature_coef)))
    grid_df = pd.DataFrame.from_dict(rf_dict, orient='index', columns=['coefficient']) \
        .sort_values('coefficient', ascending=False)

    return lr_df, rf_df, gbt_df, grid_df


def create_nan(df, title):
    nan_dict = {}
    df = df.toPandas()
    for column in df.columns:
        nan_dict[column] = df[column].values.tolist()

    index = list(nan_dict['Index'])
    b = list(nan_dict['coefficient'])
    coef = list(map(float, b))
    plt.figure(figsize=(15, 15))
    sns.barplot(index, coef)
    plt.title(title, fontsize=40)
    plt.xlabel('Features', fontsize=30)
    plt.ylabel('Feature Importance', fontsize=30)
    plt.xticks(rotation=60, fontsize=20)
    plt.yticks(fontsize=14)
    plt.show()


def plot_coef(df, title):
    nan_dict = {}
    for col in df.columns:
        nan_dict[col] = df.where(df[col].isNull() | F.isnan(df[col])).select(col).count()

    print("*******111111111111111111111111111111111111111")
    plt.figure(figsize=(30, 15))
    sns.barplot(x=df.Index, y=df.coefficient, data=nan_dict)
    plt.title(title, fontsize=40)
    plt.xlabel('Features', fontsize=30)
    plt.ylabel('Feature Importance', fontsize=30)
    plt.xticks(rotation=60, fontsize=20)
    plt.yticks(fontsize=14)
    plt.show()

