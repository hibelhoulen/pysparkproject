# import libraries
import pyspark.sql.functions as F
from pyspark.sql.types import FloatType
import pandas as pd


def save_metrics(evaluator, yhat_test):
    metrics = {}
    metrics['f1'] = evaluator.evaluate(yhat_test, {evaluator.metricName: "f1"})
    metrics['accuracy'] = evaluator.evaluate(yhat_test, {evaluator.metricName: "accuracy"})
    metrics['weighted_precision'] = evaluator.evaluate(yhat_test, {evaluator.metricName: "weightedPrecision"})
    metrics['weighted_recall'] = evaluator.evaluate(yhat_test, {evaluator.metricName: "weightedRecall"})
    metrics_df = pd.DataFrame.from_dict(metrics, orient='index', columns=['metrics'])
    conf_matrix = yhat_test.groupby("label").pivot("prediction").count().toPandas()

    return metrics_df, conf_matrix


def calculate_logloss(df):
    get_first_element = F.udf(lambda v: float(v[1]), FloatType())
    df = df.withColumn("preds", get_first_element(F.col("probability")))
    y1, y0 = F.col("label"), 1 - F.col("label")
    p1, p0 = F.col("preds"), 1 - F.col("preds")
    nll = -(y1 * F.log(p1) + y0 * F.log(p0))

    return df.agg(F.mean(nll))


def save_logloss_vals(rf_yhat_test, lr_yhat_test, gbt_yhat_test, grid_yhat_test):
    logloss = {}
    logloss['rf'] = calculate_logloss(rf_yhat_test)
    logloss['lr'] = calculate_logloss(lr_yhat_test)
    logloss['gbt'] = calculate_logloss(gbt_yhat_test)
    logloss['grid'] = calculate_logloss(grid_yhat_test)
    logloss = {'rf': 0.31383013570469653, 'lr': 2.7490194187377264, 'gbt': 0.18212699182257222, 'grid': 0.38805531522755}

    return logloss


def save_rf_metrics(evaluator, yhat_test):
    metrics = {}
    metrics['f1'] = evaluator.evaluate(yhat_test, {evaluator.metricName: "f1"})
    metrics['accuracy'] = evaluator.evaluate(yhat_test, {evaluator.metricName: "accuracy"})
    metrics['weighted_precision'] = evaluator.evaluate(yhat_test, {evaluator.metricName: "weightedPrecision"})
    metrics['weighted_recall'] = evaluator.evaluate(yhat_test, {evaluator.metricName: "weightedRecall"})
    metrics_df = pd.DataFrame.from_dict(metrics, orient='index', columns=['metrics'])
    conf_matrix = yhat_test.groupby("label").pivot("prediction").count().toPandas()

    return metrics_df, conf_matrix

