# import libraries
from pyspark.sql import SparkSession


# create a Spark session
def create_spark_session():
    spark = SparkSession\
    .builder \
    .master("local") \
    .appName("sparkify") \
    .getOrCreate()
    return spark


def load_dataset(spark, path="mini_sparkify_event_data.json"):
    print(f"\nReadinng File from: {path}")
    df = spark.read.json(path)
    print(f"\nColumns in dataset:")
    for column in df.columns:
        print(column)
    print(f"\nDatatypes in dataset: {df.persist()}")
    print(f"\nFirst DataFrame record: {df.head()}")
    return df


def load_dataset_csv(spark, path):
    print(f"\nReadinng File from: {path}")
    df = spark.read.option("header", True).csv(path)
    print(f"\nColumns in dataset:")
    for column in df.columns:
        print(column)
    print(f"\nDatatypes in dataset: {df.persist()}")
    print(f"\nFirst DataFrame record: {df.head()}")
    return df



