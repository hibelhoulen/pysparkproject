from utility.session_spark import *
from featureEngineer.cleandata import *
from featureEngineer.exploredata import *
from algo.algo import *
from metrix.metrixfunc import *
from visual.vis import *


if __name__ == '__main__':
    print("create a Spark session")
    spark = create_spark_session()
    print(spark.sparkContext.appName)
    print("****Load DATA **********")
    df = load_dataset(spark)
    print(" ----------------------------------- Clean Dataset ** explore data**------------")
    log_events = clean_func(df)
    print("***********************  explore Dataset    *************")
    log_events = create_churn_col(log_events)
    log_events = create_phase_col(log_events)
    print("**********************   modeling section     **************")
    data = spark.read.load("data/data.parquet")
    churn_vals = spark.read.load("data/churn_vals.parquet")
    train = spark.read.load("data/train.parquet")
    test = spark.read.load("data/test.parquet")
    cols = data.drop('label').drop('userId').columns
    rf_test_evaluator, rf_yhat_test, rf = results_classifier(train, test, cols, param_grid=1, rf=True)
    print("****** Fit RandomForestClassifier successfully *********** ")
    lr_test_evaluator, lr_yhat_test, lr = results_classifier(train, test, cols, param_grid=1, lr=True)
    print("****** Fit LogisticRegression successfully  ***********")
    gbt_test_evaluator, gbt_yhat_test, gbt = results_classifier(train, test, cols, param_grid=1)
    print("****** Fit GBTClassifier successfully ***********")
    cross_val, grid_evaluator, grid_yhat_test, grid_best_model = results_rf_classifier(train, test, cols, param_grid=2)

    rf_metrics, rf_matrix = save_metrics(rf_test_evaluator, rf_yhat_test)
    print("*****metrix of RandomForestClassifier*********")
    print(rf_metrics)
    print("**********metrix of LogisticRegression*******************")
    lr_metrics, lr_matrix = save_metrics(lr_test_evaluator, lr_yhat_test)
    print(lr_metrics)
    print("***********metrix of GBTClassifier**********")
    gbt_metrics, gbt_matrix = save_metrics(gbt_test_evaluator, gbt_yhat_test)
    print(gbt_metrics)
    rf_grid_metrics, rf_grid_matrix = save_rf_metrics(grid_evaluator, grid_yhat_test)
    print("****************rf_grid_metrics************************")
    print(rf_grid_metrics)

    rf_yhat_test = spark.read.load("data/rf_preds.parquet")
    lr_yhat_test = spark.read.load("data/lr_preds.parquet")
    gbt_yhat_test = spark.read.load("data/gbt_preds.parquet")
    grid_yhat_test = spark.read.load("data/rf_grid_preds.parquet")

    logloss_dict = save_logloss_vals(rf_yhat_test, lr_yhat_test, gbt_yhat_test, grid_yhat_test)
    print("************  logloss   ************")
    print(logloss_dict)
    lr_df = load_dataset_csv(spark, "data/lr_df.csv")
    rf_df = load_dataset_csv(spark, "data/rf_df.csv")
    gbt_df = load_dataset_csv(spark, "data/gbt_df.csv")
    grid_df = load_dataset_csv(spark, "data/grid_df.csv")

    create_nan(grid_df, "Random Forest Grid Search with 100 Trees")
    create_nan(lr_df, "Logistic Regression")
    create_nan(rf_df, "Random Forest - Default 20 Trees")
    create_nan(gbt_df, "Gradient Boosted Trees")

    spark.stop()
    print("fin******************")
